#RTL8188EUS
基于RTL8188EUS无线网卡的驱动程序，此驱动可工作于master模式，基于linux 3.10版本以上内核可测试通过。
使用步骤如下：
1、vim driver/rtl8188EUS_linux_v4.3.0.6_12167.20140828/Makefile
#修改成自己的配置
#CONFIG_PLATFORM_MYIMX6 = y

#ifeq ($(CONFIG_PLATFORM_MYIMX6), y)
#EXTRA_CFLAGS += -DCONFIG_LITTLE_ENDIAN
#MODULE_NAME := wlan
#KSCR := /home/pengrui/my-imx6/02_source/imx-3.14.52_1.0.o_ga/kernel/linux-3.14.52
#endif
2、导出内核目录
export ARCH=arm
/home/pengrui/my-imx6/02_source/imx-3.14.52_1.0.o_ga/tools/gcc-linaro-arm-linux-gnueabihf-4.9-2014.09_linux/bin/arm-linux-gnueabihf-
export KSRC=/home/myzr/MY-I.MX6/linux-3.0.35/
3、修改内核配置
<*>    Device Drivers > Network device support >  Wireless LAN >  IEEE 802.11 for Host AP (Prism2/2.5/3 and WEP/TKIP/CCMP)
<>     Device Drivers > Network device support >  Wireless LAN > Realtek 8187 and 8187B USB support
<>     Device Drivers > Staging drivers 
<>     Device Drivers > Network device support > Wireless LAN > Realtek rtlwifi family of devices
<>     Device Drivers > Network device support > USB Network Adapters > USB RTL8150 based ethernet device support
<>     Device Drivers > Network device support > USB Network Adapters > Realtek RTL8152/RTL8153 Based USB Ethernet Adapters
4、编译内核
make uImage
5、编译驱动
make
6、编译hostapd
vim wpa_supplicant_hostapd/wpa_supplicant_hostapd-0.8_rtw_r7475.20130812/hostapd-0.8/hostapd
#ifndef CC
CC=arm-linux-gnueabihf-gcc
#endif
7、编译hostapd
8、启动wifi脚本
insmod wlan.ko
ifconfig wlan0 192.168.1.1
hostapd -d /etc/rtl_hostapd_2G.conf -B


